var request = require('request');
var util = require('./lib/util');

exports = {
  
  events: [
    { event: "onConversationCreate", callback: "OnReplyAdded" },
  ],
  
/**
 * call an event onConversationCreate 
 */

  OnReplyAdded: function(args){

/**
 * error handling private note Added
 */

        if(args['data']['conversation']['private'] ){
          return;
        }

/**
 * call public and reply note Added
 */
      else{

        //merge and unique value cc, to and bcc emaills
        var arr1 = args ['data']['conversation']['to_emails'] == null ? [] : args ['data']['conversation']['to_emails']; 
        var arr2 = args ['data']['conversation']['bcc_emails'] == null ? [] : args ['data']['conversation']['bcc_emails']; 
        var arr3 = args ['data']['conversation']['cc_emails'] == null ? [] : args ['data']['conversation']['cc_emails'];           

        var items = arr1.concat(arr2,arr3);     //concat
        var finalemails = Array.from(new Set(items)); //unique
        console.log("-----------------------");
        console.log("arr1", arr1);
        console.log("arr2", arr2);
        console.log("arr3", arr3);
        console.log("finalemails", finalemails);

        // extract support email mapping
        var arrays = [];    
        var array = args.iparams.Mapping;
        for(var key in array){
          arrays.push(key);
        }
        console.log("arrays", arrays);

        // extract support group mapping
        var groups = [];    
        var array = args.iparams.Mapping;   
        for(var key in array){
          groups.push(array[key]);
        }
        console.log("groups", groups);
       
                 
          var ticketNumber = args['data']['conversation']['ticket_id'];  
          var url = "https://" + args.domain + "/api/v2/tickets/" + ticketNumber + "?include=requester";         
          
          request({    //get request
            url: url,
            method: "GET",
            headers: {
              Authorization: util.getFreshdeskKey(args)
            },
          },
        
        function(err, res, body) {          
          if (err) {
            console.log( err );
            return;
          }

          if(!err && res.statusCode == 200){   //success
            var res_body = JSON.parse(body); 

            // customer reply    
              if(  args['data']['conversation']['user_id'] ===  res_body.requester_id  ){

              console.log(res_body.requester_id);

              var to_emails = res_body.to_emails;    //to_email 

              //remove to_Emails bracket parenthesis
              to_emailsarray = to_emails.map(function (str) {
                if(str.indexOf("<") >= 0){
                return str.substring(str.indexOf("<") +1,str.indexOf(">") ,-1);  
                }
                return str;
              });
              console.log("to_emails of reply", to_emailsarray);              

            //remove ticket to email from final object 
            
              console.log("finalemails", finalemails);

              newarray = finalemails.map(function (str) {
                if(str.indexOf("<") >= 0){
                return str.substring(str.indexOf("<") +1,str.indexOf(">") ,-1);  
                }
                return str;
              });
              console.log("new array", newarray);              
             

              // final result
              var finalArray = newarray.filter((item) => !to_emailsarray.includes(item));
              console.log("final array", finalArray);
                 
              // mapping email with group id
              var result = finalArray.map(function(index){

                for (var key in array){     //mapping email
                if( index == key ){
                var group = array[key]; 
                var email = key;
                return {group , email};
                  }
                }
              });
              console.log("result", result);

                      
              for(var i=0; i<result.length ;i++){      //loop

                var groupid = JSON.parse(result[i].group);    //group id

                console.log(res_body.requester_id);

                //call function
                OncreateTicket(res_body,groupid);    //group id parameter
              }
            }
            else{
              console.log("This reply was not by customer");
            }
        
          }

        });
          
    }

  //ticket create
    function OncreateTicket(res_body,groupid){

      var url = "https://" + args.domain + "/api/v2/tickets" ;
      console.log(url);
      var ticketdetails = {
        subject:res_body.subject,
        description:res_body.description,
        status:res_body.status,
        priority:res_body.priority,
        requester_id:res_body.requester_id,
        group_id : groupid,
        }
       console.log(ticketdetails);
      request({                       //post request
        url: url,
        method: 'POST',
        headers: {
          Authorization: util.getFreshdeskKey(args),
          "Content-Type": "application/json"        
        },
        body:JSON.stringify(ticketdetails)
  
      }, function(err, res, body) {
        if (err) {
          console.log( err );
        }
        console.log(res.statusCode);
        console.log(body);
        if(!err && res.statusCode == 200){
          console.log('Ticket Created');
          console.log(ticketNumber +"# Ticket Assigned to group id " + rulesMatchedinBody[0].group_id +" successfully!");			  					

        }  
      });  
      }
      
          
  }    

}

